package com.okex

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class OkExApplication : Application()