package com.okex

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.okex.designsystem.theme.OkExTheme
import com.okex.instruments.InstrumentsScreen
import com.okex.instruments.InstrumentsScreenRoute
import com.okex.navigation.BottomNavItem
import com.okex.navigation.OkExNavHost
import com.okex.support.SupportScreen
import com.okex.support.SupportScreenRoute
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContent {
            OkExTheme {
                val navController = rememberNavController()
                Surface(
                    modifier = Modifier
                        .fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Scaffold(
                        modifier = Modifier.fillMaxSize(),
                        bottomBar = {
                            BottomNavigationBar(navController)
                        }) { innerPadding ->
                        OkExNavHost(Modifier.padding(innerPadding), navController = navController)
                    }
                }
            }
        }
    }
}

@Composable
fun BottomNavigationBar(navController : NavController) {
    BottomNavigation(
        modifier = Modifier.height(70.dp),
        backgroundColor = MaterialTheme.colorScheme.background,
        contentColor = MaterialTheme.colorScheme.onBackground
    ) {
        val navBackStackEntry by navController.currentBackStackEntryAsState()
        val currentRoute = navBackStackEntry?.destination?.route

        BottomNavigationItem(
            selected = currentRoute == BottomNavItem.Instruments.route,
            onClick = {
                navController.navigate(BottomNavItem.Instruments.route) {
                    popUpTo(navController.graph.startDestinationId)
                    launchSingleTop = true
                }
            },
            icon = { Icon(BottomNavItem.Instruments.icon, contentDescription = null) },
            label = { Text(BottomNavItem.Instruments.label) }
        )

        BottomNavigationItem(
            selected = currentRoute == BottomNavItem.Support.route,
            onClick = {
                navController.navigate(BottomNavItem.Support.route) {
                    popUpTo(navController.graph.startDestinationId)
                    launchSingleTop = true
                }
            },
            icon = { Icon(BottomNavItem.Support.icon, contentDescription = null) },
            label = { Text(BottomNavItem.Support.label) }
        )

    }
}