package com.okex.navigation

import androidx.compose.ui.graphics.vector.ImageVector
import com.okex.designsystem.icon.AppRoundedIcons
import com.okex.instruments.navigation.instrumentsScreenRoute
import com.okex.support.navigation.supportScreenRoute

sealed class BottomNavItem(val route: String, val icon: ImageVector, val label: String) {
    data object Instruments : BottomNavItem(instrumentsScreenRoute, AppRoundedIcons.Insights, "Instruments")
    data object Support : BottomNavItem(supportScreenRoute, AppRoundedIcons.ContactSupport, "Support")
}