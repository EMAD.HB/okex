package com.okex.navigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.okex.instruments.InstrumentsScreenRoute
import com.okex.instruments.navigation.instrumentsScreen
import com.okex.support.SupportScreenRoute
import com.okex.support.navigation.supportScreen

@Composable
fun OkExNavHost(modifier : Modifier, navController : NavHostController) {
    NavHost(
        modifier = modifier,
        navController = navController,
        startDestination = BottomNavItem.Instruments.route
    ) {
        instrumentsScreen()
        supportScreen()
    }
}