pluginManagement {
    repositories {
        google {
            content {
                includeGroupByRegex("com\\.android.*")
                includeGroupByRegex("com\\.google.*")
                includeGroupByRegex("androidx.*")
            }
        }
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}

rootProject.name = "OkEx"
include(":app")
include(":core:data")
include(":core:model")
include(":core:domain")
include(":core:designsystem")
include(":core:ui")
include(":feature:instruments")
include(":feature:support")
include(":core:network")
include(":core:common")
