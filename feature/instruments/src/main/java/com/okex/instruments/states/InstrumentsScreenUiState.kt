package com.okex.instruments.states

sealed interface InstrumentsScreenUiState {
    data object Success : InstrumentsScreenUiState
    data object Loading : InstrumentsScreenUiState
    data class Error(val reason : String) : InstrumentsScreenUiState
}