package com.okex.instruments

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.okex.common.onError
import com.okex.common.onSuccess
import com.okex.domain.cryptos.FetchCryptosListUseCase
import com.okex.domain.cryptos.FetchCryptosUpdateUseCase
import com.okex.domain.socket.SubscribeCryptoPriceUseCase
import com.okex.domain.socket.UnsubscribeCryptoPriceUseCase
import com.okex.instruments.events.InstrumentsScreenEvent
import com.okex.instruments.states.InstrumentsScreenUiState
import com.okex.model.CryptoPriceResource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class InstrumentsScreenViewModel @Inject constructor(
    private val subscribeCryptoPriceUseCase: SubscribeCryptoPriceUseCase,
    private val unsubscribeCryptoPriceUseCase: UnsubscribeCryptoPriceUseCase,
    private val fetchCryptosListUseCase: FetchCryptosListUseCase,
    private val fetchCryptosUpdateUseCase: FetchCryptosUpdateUseCase
) : ViewModel() {

    private val _uiState =
        MutableStateFlow<InstrumentsScreenUiState>(InstrumentsScreenUiState.Loading)
    val uiState: StateFlow<InstrumentsScreenUiState> = _uiState.asStateFlow()

    private val _cryptos = MutableStateFlow<List<CryptoPriceResource>?>(emptyList())
    val cryptos: StateFlow<List<CryptoPriceResource>?> = _cryptos.asStateFlow()

    init {
        fetchCryptosUpdates()
    }

    fun onEvent(event: InstrumentsScreenEvent) = when (event) {
        is InstrumentsScreenEvent.OnCryptoVisible -> subscribeTicker(event.symbol)
        is InstrumentsScreenEvent.OnCryptoInVisible -> unsubscribeTicker(event.symbol)
    }

    private fun subscribeTicker(symbol: String) {
        viewModelScope.launch { subscribeCryptoPriceUseCase.invoke(symbol) }
    }

    private fun unsubscribeTicker(symbol: String) {
        viewModelScope.launch { unsubscribeCryptoPriceUseCase.invoke(symbol) }
    }

    fun fetchTickersList() {
        viewModelScope.launch {
            fetchCryptosListUseCase.invoke(Unit)
                .onSuccess {
                    _uiState.value =
                        if (it.isEmpty()) InstrumentsScreenUiState.Loading else InstrumentsScreenUiState.Success
                    _cryptos.value = it
                }
                .onError {
                     Log.i("EMAD", "error: ${it.toString()}")
                    _uiState.value = InstrumentsScreenUiState.Error(it.message ?: "Unknown error")
                }
        }
    }

    private fun fetchCryptosUpdates() {
        viewModelScope.launch {
            fetchCryptosUpdateUseCase.invoke(Unit).collect { resource ->
                resource
                    .onSuccess { it?.let { updateCryptoList(it) } }
                    .onError {
                        Log.i("EMAD", "error: ${it.toString()}")
                        _uiState.value = InstrumentsScreenUiState.Error(it.message.toString())
                    }
            }
        }
    }

    private fun updateCryptoList(crypto: CryptoPriceResource) {
        _cryptos.update { currentCryptos ->
            currentCryptos?.map { if (it.symbol == crypto.symbol) crypto else it }
        }
    }
}
