package com.okex.instruments.navigation

import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import com.okex.instruments.InstrumentsScreenRoute

const val instrumentsScreenRoute = "instruments_screen_route"

fun NavGraphBuilder.instrumentsScreen() {
    composable(route = instrumentsScreenRoute) {
        InstrumentsScreenRoute()
    }
}
