package com.okex.instruments.events

sealed interface InstrumentsScreenEvent {

    data class OnCryptoVisible(val symbol : String) : InstrumentsScreenEvent
    data class OnCryptoInVisible(val symbol : String) : InstrumentsScreenEvent

}