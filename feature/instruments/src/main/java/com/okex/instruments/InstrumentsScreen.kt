package com.okex.instruments

import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Shapes
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.okex.common.isInternetConnected
import com.okex.designsystem.component.BodyTextField
import com.okex.designsystem.component.TextBodyMedium
import com.okex.designsystem.component.TextTitleMedium
import com.okex.designsystem.icon.AppRoundedIcons
import com.okex.designsystem.theme.TypographyType
import com.okex.instruments.events.InstrumentsScreenEvent
import com.okex.instruments.states.InstrumentsScreenUiState
import com.okex.model.CryptoPriceResource
import com.okex.ui.CryptoItem
import com.okex.ui.LoadingAnimation
import com.okex.ui.NoInternetConnection
import com.okex.ui.ToHorizontalSpace
import com.okex.ui.ToVerticalSpace
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map

@Composable
fun InstrumentsScreenRoute(
    viewModel: InstrumentsScreenViewModel = hiltViewModel()
) {
    InstrumentsScreen(viewModel)
}

@Composable
fun InstrumentsScreen(
    viewModel: InstrumentsScreenViewModel
) {
    val uiState by viewModel.uiState.collectAsState()
    val instrumentType = remember {
        mutableStateOf("USDT")
    }
    val showInternetConnection = remember {
        mutableStateOf(false)
    }
    val context = LocalContext.current
    LaunchedEffect(key1 = context.isInternetConnected()) {
        showInternetConnection.value = context.isInternetConnected()
        if (showInternetConnection.value) viewModel.fetchTickersList()
    }
    if (!showInternetConnection.value) NoInternetConnection()
    else Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(10.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.End,
                verticalAlignment = Alignment.CenterVertically
            ) {
                TextTitleMedium(
                    text = stringResource(R.string.select_coin),
                    color = MaterialTheme.colorScheme.onBackground
                )
                Icon(
                    imageVector = AppRoundedIcons.Filter,
                    contentDescription = AppRoundedIcons.Filter.name,
                    tint = MaterialTheme.colorScheme.primary
                )
            }
            14.ToVerticalSpace()
            when (uiState) {
                is InstrumentsScreenUiState.Error -> {
                    NoInternetConnection()
                    Toast.makeText(
                        context,
                        (uiState as InstrumentsScreenUiState.Error).reason,
                        Toast.LENGTH_SHORT
                    ).show()
                }

                is InstrumentsScreenUiState.Loading -> LoadingAnimation()

                is InstrumentsScreenUiState.Success -> {
                    TabLayout(instrumentType.value)
                    14.ToVerticalSpace()
                    SearchBox()
                    24.ToVerticalSpace()
                    CryptosHeader()
                    12.ToVerticalSpace()
                    CryptosList(viewModel)
                }
            }
        }
}

@Composable
fun TabLayout(
    instrumentType: String
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .background(
                color = MaterialTheme.colorScheme.outlineVariant.copy(0.2f),
                shape = Shapes().extraSmall
            )
            .padding(vertical = 4.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceAround
    ) {
        Box(
            modifier = Modifier
                .fillMaxWidth(0.47f)
                .height(26.dp)
                .background(
                    color = when (instrumentType) {
                        "IRT" -> MaterialTheme.colorScheme.background
                        "USDT" -> Color.Transparent
                        else -> Color.Transparent
                    },
                    shape = Shapes().extraSmall
                )
        ) {
            TextBodyMedium(
                modifier = Modifier.align(Alignment.Center),
                typographyType = TypographyType.ENGLISH,
                text = "IRT",
                color = MaterialTheme.colorScheme.onBackground
            )
        }
        4.ToHorizontalSpace()
        Box(
            modifier = Modifier
                .fillMaxWidth(0.9f)
                .height(26.dp)
                .background(
                    color = when (instrumentType) {
                        "USDT" -> MaterialTheme.colorScheme.background
                        "IRT" -> Color.Transparent
                        else -> Color.Transparent
                    },
                    shape = Shapes().extraSmall
                )
        ) {
            TextBodyMedium(
                modifier = Modifier.align(Alignment.Center),
                typographyType = TypographyType.ENGLISH,
                text = "USDT",
                color = MaterialTheme.colorScheme.onBackground
            )
        }
    }
}

@Composable
fun SearchBox() {
    val searchQuery = remember {
        mutableStateOf("")
    }
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .background(
                MaterialTheme.colorScheme.outlineVariant.copy(0.2f),
                shape = Shapes().extraSmall
            )
            .padding(6.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Icon(
            imageVector = AppRoundedIcons.Close,
            contentDescription = AppRoundedIcons.Close.name,
            tint = MaterialTheme.colorScheme.outlineVariant
        )
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.End
        ) {
            BodyTextField(
                modifier = Modifier.fillMaxWidth(0.5f),
                placeHolder = stringResource(R.string.search_hint),
                value = searchQuery.value,
                singleLine = true,
                onValueChange = { newSearchQuery ->
                    searchQuery.value = newSearchQuery
                }
            )
            4.ToHorizontalSpace()
            Icon(
                imageVector = AppRoundedIcons.Search,
                contentDescription = AppRoundedIcons.Search.name,
                tint = MaterialTheme.colorScheme.outlineVariant
            )
        }

    }
}

@Composable
fun CryptosHeader() {
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        TextTitleMedium(
            text = stringResource(R.string.currency_pairs),
            color = MaterialTheme.colorScheme.outlineVariant
        )
        TextTitleMedium(
            text = stringResource(R.string.last_24_hours_changes),
            color = MaterialTheme.colorScheme.outlineVariant
        )
        TextTitleMedium(
            text = stringResource(R.string.price),
            color = MaterialTheme.colorScheme.outlineVariant
        )
    }
}

@Composable
fun CryptosList(
    viewModel: InstrumentsScreenViewModel,
    listState: LazyListState = rememberLazyListState()
) {
    val cryptos = viewModel.cryptos.collectAsState().value ?: emptyList()

    LazyColumn(state = listState) {
        items(cryptos, key = { crypto -> crypto.symbol }) { crypto ->
            CryptoItem(crypto)
        }
    }
    ManageCryptoSubscription(
        listState,
        cryptos,
        onVisibleCryptoItem = { cryptoSymbol ->
            viewModel.onEvent(InstrumentsScreenEvent.OnCryptoVisible(cryptoSymbol))
        },
        onInVisibleCryptoItem = { cryptoSymbol ->
            viewModel.onEvent(InstrumentsScreenEvent.OnCryptoInVisible(cryptoSymbol))
        }
    )
}

@Composable
private fun ManageCryptoSubscription(
    listState: LazyListState,
    cryptoLists: List<CryptoPriceResource>,
    onVisibleCryptoItem: (cryptoSymbol: String) -> Unit,
    onInVisibleCryptoItem: (cryptoSymbol: String) -> Unit
) {
    val visibleCryptos = remember { mutableSetOf<String>() }
    LaunchedEffect(listState) {
        snapshotFlow {
            val firstVisibleCryptoItemIndex = listState.firstVisibleItemIndex
            val lastVisibleCryptoItemIndex =
                listState.layoutInfo.visibleItemsInfo.lastOrNull()?.index
                    ?: firstVisibleCryptoItemIndex
            firstVisibleCryptoItemIndex to lastVisibleCryptoItemIndex
        }
            .distinctUntilChanged()
            .map { (firstIndex, lastIndex) ->
                if (cryptoLists.isNotEmpty())
                    cryptoLists.subList(
                        firstIndex,
                        kotlin.math.min(lastIndex + 1, cryptoLists.size)
                    )
                        .map { it.symbol }.toSet()
                else emptySet()
            }
            .collect { visibleCryptosIndex ->
                val newSubscriptions = visibleCryptosIndex - visibleCryptos
                val newUnSubscriptions = visibleCryptos - visibleCryptosIndex

                newSubscriptions.forEach { cryptoSymbol ->
                    onVisibleCryptoItem(cryptoSymbol)
                }
                newUnSubscriptions.forEach { cryptoSymbol ->
                    onInVisibleCryptoItem(cryptoSymbol)
                }
                visibleCryptos.run {
                    clear()
                    addAll(visibleCryptosIndex)
                }
            }
    }
}