package com.okex.support

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.okex.common.onError
import com.okex.common.onSuccess
import com.okex.domain.qa.FetchQaQuestionsUseCase
import com.okex.support.state.SupportScreenUiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SupportScreenViewModel @Inject constructor(
    private val fetchQaQuestionsUseCase : FetchQaQuestionsUseCase
) : ViewModel() {

    private val _uiState = MutableStateFlow<SupportScreenUiState>(SupportScreenUiState.Loading)
    val uiState : StateFlow<SupportScreenUiState> = _uiState

    fun fetchQaQuestions() {
        viewModelScope.launch {
            fetchQaQuestionsUseCase.invoke(Unit)
                .onSuccess { qaQuestions ->
                    _uiState.update { SupportScreenUiState.Success(qaQuestions) }
                }
                .onError { exception ->
                    _uiState.update { SupportScreenUiState.Error(exception.message ?: "Unknown error") }
                }
        }
    }

}