package com.okex.support.navigation

import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import com.okex.support.SupportScreenRoute

const val supportScreenRoute = "support_screen_route"

fun NavGraphBuilder.supportScreen() {
    composable(route = supportScreenRoute) {
        SupportScreenRoute()
    }
}
