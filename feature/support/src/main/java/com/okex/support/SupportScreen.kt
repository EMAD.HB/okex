package com.okex.support

import android.widget.Toast
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Shapes
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.okex.common.isInternetConnected
import com.okex.designsystem.component.TextBodyLarge
import com.okex.designsystem.component.TextBodyMedium
import com.okex.designsystem.component.TextTitleLarge
import com.okex.designsystem.icon.AppRoundedIcons
import com.okex.designsystem.theme.TypographyType
import com.okex.model.qa.QaResource
import com.okex.support.state.SupportScreenUiState
import com.okex.ui.LoadingAnimation
import com.okex.ui.NoInternetConnection

@Composable
fun SupportScreenRoute(
    viewModel: SupportScreenViewModel = hiltViewModel()
) {
    val uiState by viewModel.uiState.collectAsState()
    SupportScreen(uiState, viewModel)
}

@Composable
fun SupportScreen(uiState: SupportScreenUiState, viewModel: SupportScreenViewModel) {
    val showInternetConnection = remember {
        mutableStateOf(false)
    }
    val context = LocalContext.current
    LaunchedEffect(key1 = context.isInternetConnected()) {
        showInternetConnection.value = context.isInternetConnected()
        if (showInternetConnection.value) viewModel.fetchQaQuestions()
    }
    if (!showInternetConnection.value) NoInternetConnection()
    else Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState()),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        when (uiState) {
            is SupportScreenUiState.Success -> QaQuestionsList(uiState.data)
            is SupportScreenUiState.Loading -> LoadingAnimation()
            is SupportScreenUiState.Error -> {
                NoInternetConnection()
                Toast.makeText(context, uiState.reason, Toast.LENGTH_LONG).show()
            }
        }
    }
}

@Composable
fun QaQuestionsList(
    questions: List<QaResource>
) {
    questions.forEach {
        TextTitleLarge(
            modifier = Modifier
                .fillMaxWidth()
                .padding(12.dp),
            text = it.sectionTitle,
            typographyType = TypographyType.PERSIAN,
            color = MaterialTheme.colorScheme.onBackground
        )
        it.questions.forEach { qa ->
            QaQuestionItem(question = qa.question, answer = qa.answer)
        }
    }
}

@Composable
fun QaQuestionItem(question: String, answer: String) {
    var expandedState by remember { mutableStateOf(false) }
    val interactionSource = remember { MutableInteractionSource() }
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 6.dp, horizontal = 10.dp)
            .background(MaterialTheme.colorScheme.primaryContainer, Shapes().small)
            .padding(vertical = 16.dp, horizontal = 12.dp)
            .clickable(
                interactionSource = interactionSource,
                indication = null
            ) { expandedState = !expandedState },
        horizontalAlignment = Alignment.Start
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                imageVector = AppRoundedIcons.ArrowLeft,
                contentDescription = AppRoundedIcons.ArrowLeft.name,
                tint = MaterialTheme.colorScheme.onPrimaryContainer
            )
            TextBodyLarge(
                modifier = Modifier.fillMaxWidth(),
                text = question,
                typographyType = TypographyType.PERSIAN,
                color = MaterialTheme.colorScheme.onPrimaryContainer
            )
        }
        AnimatedVisibility(
            visible = expandedState,
            enter = fadeIn() + slideInVertically(),
            exit = fadeOut() + slideOutVertically()
        ) {
            TextBodyMedium(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 12.dp),
                text = answer,
                typographyType = TypographyType.PERSIAN,
                color = MaterialTheme.colorScheme.onPrimaryContainer.copy(0.5f)
            )
        }
    }
}