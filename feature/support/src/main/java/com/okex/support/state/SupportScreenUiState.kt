package com.okex.support.state

import com.okex.model.qa.QaResource

sealed interface SupportScreenUiState {

    data class Error(val reason : String) : SupportScreenUiState

    data object Loading : SupportScreenUiState

    data class Success(val data : List<QaResource>) : SupportScreenUiState

}