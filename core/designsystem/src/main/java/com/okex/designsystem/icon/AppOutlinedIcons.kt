package com.okex.designsystem.icon

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Delete
import androidx.compose.material.icons.outlined.Settings

object AppOutlinedIcons {
    val Settings = Icons.Outlined.Settings
    val Delete = Icons.Outlined.Delete
}