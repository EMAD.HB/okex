package com.okex.designsystem.icon

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.rounded.ContactSupport
import androidx.compose.material.icons.rounded.Add
import androidx.compose.material.icons.rounded.AddCircleOutline
import androidx.compose.material.icons.rounded.ArrowBackIosNew
import androidx.compose.material.icons.rounded.Close
import androidx.compose.material.icons.rounded.ContactSupport
import androidx.compose.material.icons.rounded.FilterAlt
import androidx.compose.material.icons.rounded.Insights
import androidx.compose.material.icons.rounded.Search

object AppRoundedIcons {
    val Add = Icons.Rounded.Add
    val ArrowLeft = Icons.Rounded.ArrowBackIosNew
    val Filter = Icons.Rounded.FilterAlt
    val Insights = Icons.Rounded.Insights
    val ContactSupport = Icons.AutoMirrored.Rounded.ContactSupport
    val CircleAdd = Icons.Rounded.AddCircleOutline
    val Search = Icons.Rounded.Search
    val Close = Icons.Rounded.Close
}