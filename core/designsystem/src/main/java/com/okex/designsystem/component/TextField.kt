package com.okex.designsystem.component

import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDirection
import com.okex.designsystem.theme.TypographyPersian
import com.okex.designsystem.theme.TypographyType

@Composable
fun TitleTextField(
    modifier : Modifier = Modifier,
    placeHolder : String,
    value : String,
    onValueChange : (String) -> Unit
) {
    BasicTextField(
        value = value,
        onValueChange = onValueChange,
        modifier = modifier,
        textStyle = TypographyPersian.headlineMedium.copy(color = MaterialTheme.colorScheme.onBackground),
        decorationBox = {
            value.ifEmpty {
                TextHeaderMedium(
                    text = placeHolder,
                    color = MaterialTheme.colorScheme.outlineVariant,
                    typographyType = TypographyType.PERSIAN
                )
            }
            it()
        }
    )
}

@Composable
fun BodyTextField(
    modifier : Modifier = Modifier,
    placeHolder : String,
    value : String,
    textDirection : TextDirection = TextDirection.Rtl,
    textAlign : TextAlign = TextAlign.End,
    maxLine : Int = Int.MAX_VALUE,
    singleLine : Boolean = false,
    onValueChange : (String) -> Unit
) {
    BasicTextField(
        value = value,
        onValueChange = onValueChange,
        modifier = modifier,
        textStyle = TypographyPersian.bodyLarge.copy(
            color = MaterialTheme.colorScheme.onSurfaceVariant,
            textDirection = textDirection,
            textAlign = textAlign
        ),
        maxLines = maxLine,
        singleLine = singleLine,
        decorationBox = {
            value.ifEmpty {
                TextBodyLarge(text = placeHolder, color = MaterialTheme.colorScheme.outline)
            }
            it()
        }
    )
}