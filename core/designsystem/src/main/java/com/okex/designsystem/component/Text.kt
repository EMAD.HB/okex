package com.okex.designsystem.component

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDirection
import androidx.compose.ui.text.style.TextOverflow
import com.okex.designsystem.theme.TypographyEnglish
import com.okex.designsystem.theme.TypographyPersian
import com.okex.designsystem.theme.TypographyType

@Composable
fun TextHeaderLarge(
    modifier : Modifier = Modifier,
    text : String,
    typographyType : TypographyType,
    color : Color
) {
    Text(
        text = text,
        modifier = modifier,
        style = when(typographyType){
            TypographyType.PERSIAN -> TypographyPersian.headlineLarge.copy(
                color = color
            )
            TypographyType.ENGLISH -> TypographyEnglish.headlineLarge.copy(
                color = color
            )
        }
    )
}

@Composable
fun TextHeaderMedium(
    modifier : Modifier = Modifier,
    text : String,
    maxLine : Int = Int.MAX_VALUE,
    typographyType : TypographyType = TypographyType.PERSIAN,
    color : Color,
    ellipsis : TextOverflow = TextOverflow.Clip
) {
    Text(
        text = text,
        modifier = modifier,
        maxLines = maxLine,
        style = when(typographyType){
            TypographyType.PERSIAN -> TypographyPersian.headlineMedium.copy(
                color = color
            )
            TypographyType.ENGLISH -> TypographyEnglish.headlineMedium.copy(
                color = color
            )
        },
        overflow = ellipsis
    )
}

@Composable
fun TextHeaderSmall(
    modifier : Modifier = Modifier,
    text : String,
    typographyType : TypographyType,
    color : Color
) {
    Text(
        text = text,
        modifier = modifier,
        style = when(typographyType){
            TypographyType.PERSIAN -> TypographyPersian.headlineSmall.copy(
                color = color
            )
            TypographyType.ENGLISH -> TypographyEnglish.headlineSmall.copy(
                color = color
            )
        }
    )
}

@Composable
fun TextTitleLarge(
    modifier : Modifier = Modifier,
    text : String,
    textAlign : TextAlign = TextAlign.Start,
    direction : TextDirection = TextDirection.Rtl,
    typographyType : TypographyType,
    color : Color
) {
    Text(
        text = text,
        modifier = modifier,
        textAlign = textAlign,
        style = when(typographyType){
            TypographyType.PERSIAN -> TypographyPersian.titleLarge.copy(
                color = color,
                textDirection = direction
            )
            TypographyType.ENGLISH -> TypographyEnglish.titleLarge.copy(
                color = color,
                textDirection = direction
            )
        }
    )
}

@Composable
fun TextTitleMedium(
    modifier : Modifier = Modifier,
    text : String,
    typographyType : TypographyType = TypographyType.PERSIAN,
    color : Color
) {
    Text(
        text = text,
        modifier = modifier,
        style = when(typographyType){
            TypographyType.PERSIAN -> TypographyPersian.titleMedium.copy(
                color = color
            )
            TypographyType.ENGLISH -> TypographyEnglish.titleMedium.copy(
                color = color
            )
        }
    )
}

@Composable
fun TextTitleSmall(
    modifier : Modifier = Modifier,
    text : String,
    typographyType : TypographyType,
    color : Color
) {
    Text(
        text = text,
        modifier = modifier,
        style = when(typographyType){
            TypographyType.PERSIAN -> TypographyPersian.titleSmall.copy(
                color = color
            )
            TypographyType.ENGLISH -> TypographyEnglish.titleSmall.copy(
                color = color
            )
        }
    )
}

@Composable
fun TextBodyLarge(
    modifier : Modifier = Modifier,
    text : String,
    typographyType : TypographyType = TypographyType.PERSIAN,
    textAlign : TextAlign = TextAlign.Start,
    direction : TextDirection = TextDirection.Rtl,
    color : Color
) {
    Text(
        text = text,
        modifier = modifier,
        style = when(typographyType){
            TypographyType.PERSIAN -> TypographyPersian.bodyLarge.copy(
                color = color,
                textDirection = direction,
                textAlign = textAlign
            )
            TypographyType.ENGLISH -> TypographyEnglish.bodyLarge.copy(
                color = color,
                textDirection = direction,
                textAlign = textAlign
            )
        }
    )
}

@Composable
fun TextBodyMedium(
    modifier : Modifier = Modifier,
    text : String,
    maxLine : Int = Int.MAX_VALUE,
    typographyType : TypographyType = TypographyType.PERSIAN,
    textAlign : TextAlign = TextAlign.Start,
    direction : TextDirection = TextDirection.Rtl,
    color : Color,
    ellipsis : TextOverflow = TextOverflow.Clip
) {
    Text(
        text = text,
        modifier = modifier,
        maxLines = maxLine,
        textAlign = textAlign,
        style = when(typographyType){
            TypographyType.PERSIAN -> TypographyPersian.bodyMedium.copy(
                color = color,
                textDirection = direction
            )
            TypographyType.ENGLISH -> TypographyEnglish.bodyMedium.copy(
                color = color,
                textDirection = direction
            )
        } ,
        overflow = ellipsis
    )
}

@Composable
fun TextBodySmall(
    modifier : Modifier = Modifier,
    text : String,
    typographyType : TypographyType,
    color : Color
) {
    Text(
        text = text,
        modifier = modifier,
        style = when(typographyType){
            TypographyType.PERSIAN -> TypographyPersian.bodySmall.copy(
                color = color
            )
            TypographyType.ENGLISH -> TypographyEnglish.bodySmall.copy(
                color = color
            )
        }
    )
}

@Composable
fun TextLabelLarge(
    modifier : Modifier = Modifier,
    text : String,
    typographyType : TypographyType,
    color : Color
) {
    Text(
        text = text,
        modifier = modifier,
        style = when(typographyType){
            TypographyType.PERSIAN -> TypographyPersian.labelLarge.copy(
                color = color
            )
            TypographyType.ENGLISH -> TypographyEnglish.labelLarge.copy(
                color = color
            )
        }
    )
}

@Composable
fun TextLabelMedium(
    modifier : Modifier = Modifier,
    text : String,
    typographyType : TypographyType,
    color : Color
) {
    Text(
        text = text,
        modifier = modifier,
        style = when(typographyType){
            TypographyType.PERSIAN -> TypographyPersian.labelMedium.copy(
                color = color
            )
            TypographyType.ENGLISH -> TypographyEnglish.labelMedium.copy(
                color = color
            )
        }
    )
}

@Composable
fun TextLabelSmall(
    modifier : Modifier = Modifier,
    text : String,
    typographyType : TypographyType,
    color : Color
) {
    Text(
        text = text,
        modifier = modifier,
        style = when(typographyType){
            TypographyType.PERSIAN -> TypographyPersian.labelSmall.copy(
                color = color
            )
            TypographyType.ENGLISH -> TypographyEnglish.labelSmall.copy(
                color = color
            )
        }
    )
}