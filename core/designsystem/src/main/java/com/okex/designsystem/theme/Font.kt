package com.okex.designsystem.theme

import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import com.okex.designsystem.R

val yekanFamily = FontFamily(
    Font(R.font.yekan, FontWeight.Light),
    Font(R.font.yekan, FontWeight.Normal),
    Font(R.font.yekan, FontWeight.Normal, FontStyle.Italic),
    Font(R.font.yekan, FontWeight.Medium),
    Font(R.font.yekan, FontWeight.Bold)
)