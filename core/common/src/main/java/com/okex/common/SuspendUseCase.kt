package com.okex.common

import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

abstract class SuspendUseCase<in P, R>(private val coroutineDispatcher: CoroutineContext) {
    suspend operator fun invoke(parameter: P): Resource<R> {
        return try {
            withContext(coroutineDispatcher) {
                execute(parameter).let {
                    Resource.Success(it)
                }
            }
        } catch (e : Exception){
            Resource.Error(e)
        }
    }
    @Throws(RuntimeException::class)
    protected abstract suspend fun execute(parameters: P): R
}