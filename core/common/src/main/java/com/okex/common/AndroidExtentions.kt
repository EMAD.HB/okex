package com.okex.common

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import retrofit2.Response

fun Context.isInternetConnected(): Boolean {
    val connectivityManager =
        getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    val network = connectivityManager.activeNetwork
    val networkCapabilities = connectivityManager.getNetworkCapabilities(network)

    return networkCapabilities != null &&
            (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
                    networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR))
}

fun <T, R> Response<T>.toApiResponse(transform: (T) -> R): ApiResponse<R> {
    return if (isSuccessful) {
        val body = body()
        if (body != null) {
            ApiResponse.Success(transform(body))
        } else {
            ApiResponse.Error("Response body is null")
        }
    } else {
        ApiResponse.Error(errorBody()?.toString() ?: "Unknown error")
    }
}

sealed class ApiResponse<T> {
    data class Success<T>(val data: T) : ApiResponse<T>()
    data class Error<T>(val errorMessage: String) : ApiResponse<T>()

    companion object {
        fun <T> create(response: Response<T>): ApiResponse<T> {
            return if (response.isSuccessful) {
                val body = response.body()
                if (body != null) {
                    Success(body)
                } else {
                    Error("Response body is null")
                }
            } else {
                Error(response.errorBody()?.toString() ?: "Unknown error")
            }
        }

        fun <T> create(throwable: Throwable): Error<T> {
            return Error(throwable.message ?: "Unknown error")
        }
    }
}
