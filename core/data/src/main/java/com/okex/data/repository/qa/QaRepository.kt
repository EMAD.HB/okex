package com.okex.data.repository.qa

import com.okex.model.qa.QaResource

interface QaRepository {

    suspend fun fetchQaQuestions() : List<QaResource>

}