package com.okex.data.repository.crypto

import com.okex.data.utils.formatMessage
import com.okex.model.CryptoPriceResource
import com.okex.network.socket.CryptoWebSocketClient
import com.okex.network.socket.constants.CryptoWebSocketOperations
import com.okex.network.socket.model.toCryptoPriceResource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class CryptoRepositoryImpl @Inject constructor(
    private val webSocketClient: CryptoWebSocketClient
) : CryptoRepository {

    init {
        webSocketClient.start()
    }

    override fun disconnect() {
        webSocketClient.stop()
    }

    override fun fetchCryptoPrice(): Flow<CryptoPriceResource> =
        webSocketClient.cryptoDataFlow.map {
            it.toCryptoPriceResource()
        }

    override fun subscribeToCryptoPrice(symbol: String) {
        webSocketClient.send(formatMessage(CryptoWebSocketOperations.SUBSCRIBE.value,symbol))
    }

    override fun unSubscribeToCryptoPrice(symbol: String) {
        webSocketClient.send(formatMessage(CryptoWebSocketOperations.UNSUBSCRIBE.value,symbol))
    }

}