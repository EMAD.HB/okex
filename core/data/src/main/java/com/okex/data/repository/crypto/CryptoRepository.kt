package com.okex.data.repository.crypto

import com.okex.model.CryptoPriceResource
import kotlinx.coroutines.flow.Flow

interface CryptoRepository {

    fun disconnect()
    fun fetchCryptoPrice() : Flow<CryptoPriceResource>
    fun subscribeToCryptoPrice(symbol: String)
    fun unSubscribeToCryptoPrice(symbol: String)

}