package com.okex.data.di

import com.okex.data.repository.crypto.CryptoRepository
import com.okex.data.repository.crypto.CryptoRepositoryImpl
import com.okex.data.repository.qa.QaRepository
import com.okex.data.repository.qa.QaRepositoryImpl
import com.okex.data.repository.tickers.TickersRepository
import com.okex.data.repository.tickers.TickersRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface RepositoryModule {

    @Binds
    fun bindCryptoRepositoryToCryptoRepositoryImpl(impl : CryptoRepositoryImpl) : CryptoRepository

    @Binds
    fun bindQaRepositoryToQaRepositoryImpl(impl : QaRepositoryImpl) : QaRepository

    @Binds
    fun bindTickersRepositoryToTickersRepositoryImpl(impl : TickersRepositoryImpl) : TickersRepository
}