package com.okex.data.repository.qa

import com.okex.common.ApiResponse
import com.okex.common.toApiResponse
import com.okex.model.qa.QaResource
import com.okex.network.retrofit.QaApiService
import com.okex.network.retrofit.model.qa.toQaResource
import javax.inject.Inject

class QaRepositoryImpl @Inject constructor(
    private val qaApiService: QaApiService
) : QaRepository {

    override suspend fun fetchQaQuestions(): List<QaResource> {
        val response = qaApiService.getQaQuestions().toApiResponse {
            it.data.map { qaResource -> qaResource.toQaResource() }
        }
        return when(response) {
            is ApiResponse.Success -> response.data
            is ApiResponse.Error -> throw Exception(response.errorMessage)
        }
    }
}