package com.okex.data.repository.tickers

import com.okex.common.ApiResponse
import com.okex.common.toApiResponse
import com.okex.model.CryptoPriceResource
import com.okex.model.toCryptoPriceResource
import com.okex.network.retrofit.TickersApiService
import com.okex.network.retrofit.model.ticker.toTickersResource
import javax.inject.Inject

class TickersRepositoryImpl @Inject constructor(
    private val tickersApiService: TickersApiService
) : TickersRepository {

    override suspend fun getTickers(): List<CryptoPriceResource> {
        val response = tickersApiService.getTickers().toApiResponse {
            it.toTickersResource().map { ticker -> ticker.toCryptoPriceResource() }
        }
        return when(response){
            is ApiResponse.Success -> response.data
            is ApiResponse.Error -> throw Exception(response.errorMessage)
        }
    }
}