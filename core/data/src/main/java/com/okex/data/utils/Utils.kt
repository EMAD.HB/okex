package com.okex.data.utils

fun formatMessage(operation: String, symbol: String): String {
    return """{"op": "$operation", "args": [{"channel": "tickers", "instId": "$symbol"}]}"""
}