package com.okex.data.repository.tickers

import com.okex.model.CryptoPriceResource

interface TickersRepository {

    suspend fun getTickers() : List<CryptoPriceResource>

}