package com.okex.domain.cryptos

import com.okex.common.FlowUseCase
import com.okex.common.Resource
import com.okex.data.repository.crypto.CryptoRepository
import com.okex.model.CryptoPriceResource
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class FetchCryptosUpdateUseCase @Inject constructor(
    private val repository: CryptoRepository
) : FlowUseCase<Unit , CryptoPriceResource?>(IO) {

    override fun execute(parameter: Unit): Flow<Resource<CryptoPriceResource?>> =
        repository.fetchCryptoPrice().map { Resource.Success(it) }

}