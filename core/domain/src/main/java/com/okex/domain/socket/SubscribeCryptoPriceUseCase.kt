package com.okex.domain.socket

import com.okex.common.SuspendUseCase
import com.okex.data.repository.crypto.CryptoRepository
import kotlinx.coroutines.Dispatchers.IO
import javax.inject.Inject

class SubscribeCryptoPriceUseCase @Inject constructor(
    private val repository : CryptoRepository
) : SuspendUseCase<String , Unit>(IO) {

    override suspend fun execute(parameters: String) =
        repository.subscribeToCryptoPrice(parameters)

}