package com.okex.domain.cryptos

import com.okex.common.SuspendUseCase
import com.okex.data.repository.tickers.TickersRepository
import com.okex.model.CryptoPriceResource
import kotlinx.coroutines.Dispatchers.IO
import javax.inject.Inject

class FetchCryptosListUseCase @Inject constructor(
    private val repository: TickersRepository
) : SuspendUseCase<Unit, List<CryptoPriceResource>>(IO) {

    override suspend fun execute(parameters: Unit): List<CryptoPriceResource> =
        repository.getTickers()

}