package com.okex.domain.qa

import com.okex.common.SuspendUseCase
import com.okex.data.repository.qa.QaRepository
import com.okex.model.qa.QaResource
import kotlinx.coroutines.Dispatchers.IO
import javax.inject.Inject

class FetchQaQuestionsUseCase @Inject constructor(
    private val repository : QaRepository
) : SuspendUseCase<Unit , List<QaResource>>(IO) {

    override suspend fun execute(parameters: Unit): List<QaResource> = repository.fetchQaQuestions()

}