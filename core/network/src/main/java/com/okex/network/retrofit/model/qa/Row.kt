package com.okex.network.retrofit.model.qa

import com.okex.model.qa.QaQuestionsResource

data class Row(
    val _id: String,
    val answer: String,
    val question: String
)

fun Row.toQaQuestions() = QaQuestionsResource(
    question = this.question,
    answer = answer
)