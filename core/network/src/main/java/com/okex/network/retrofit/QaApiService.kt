package com.okex.network.retrofit

import com.okex.network.retrofit.model.qa.QARemoteDataModel
import retrofit2.Response
import retrofit2.http.GET

interface QaApiService {
    @GET("/server/api/support/faq")
    suspend fun getQaQuestions(): Response<QARemoteDataModel>
}