package com.okex.network.retrofit.model.qa

import com.okex.model.qa.QaQuestionsResource

data class Children(
    val _id: String,
    val category: String,
    val rows: List<Row>
)