package com.okex.network.socket.constants

enum class CryptoWebSocketOperations(val value: String) {
    SUBSCRIBE("subscribe"),
    UNSUBSCRIBE("unsubscribe")
}