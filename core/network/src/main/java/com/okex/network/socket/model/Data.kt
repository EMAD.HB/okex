package com.okex.network.socket.model

data class Data(
    val askPx: String,
    val askSz: String,
    val bidPx: String,
    val bidSz: String,
    val high24h: String,
    val instId: String,
    val last: String,
    val lastSz: String,
    val low24h: String,
    val open24h: String,
    val sodUtc0: String,
    val ts: Long,
    val vol24h: String,
    val volCcy24h: String
)