package com.okex.network.retrofit.model.qa

data class QARemoteDataModel(
    val `data`: List<Data>,
    val status: Boolean
)