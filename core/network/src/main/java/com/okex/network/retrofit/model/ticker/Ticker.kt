package com.okex.network.retrofit.model.ticker

data class Ticker(
    val best_ask: String,
    val best_bid: String,
    val high_24h: Double,
    val last: String,
    val low_24h: Double,
    val open_24h: String,
    val symbol: String,
    val ts: Long,
    val vol_24h: Double,
    val vol_24h_pair: Double
)