package com.okex.network.retrofit.model.qa

import com.okex.model.qa.QaResource

data class Data(
    val __v: Int,
    val _id: String,
    val category: String,
    val children: List<Children>,
    val name: String
)

fun Data.toQaResource() = QaResource(
    sectionTitle = this.category,
    questions = children.flatMap { children ->
        children.rows.map { questionRow ->
            questionRow.toQaQuestions()
        }
    }
)