package com.okex.network.socket

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.okex.network.socket.constants.CryptoWebSocketClientConstants.CRYPTO_WEBSOCKET_CLIENT_TAG
import com.okex.network.socket.model.CryptoPriceNetworkModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.WebSocket
import okhttp3.WebSocketListener

class DefaultCryptoWebSocketClient(
    private val url: String,
    private val gson: Gson
) : CryptoWebSocketClient, WebSocketListener() {

    private var webSocket: WebSocket? = null
    private val client = OkHttpClient()
    private val coroutineScope = CoroutineScope(SupervisorJob() + IO)
    private var isConnected = false

    private val _cryptoDataFlow = MutableSharedFlow<CryptoPriceNetworkModel>()
    override val cryptoDataFlow: SharedFlow<CryptoPriceNetworkModel> = _cryptoDataFlow

    override fun start() {
        val request = Request.Builder().url(url).build()
        webSocket = client.newWebSocket(request, this)
    }

    override fun stop() {
        webSocket?.close(1000, "Client initiated closure")
        coroutineScope.cancel("Stopping the client")
        client.dispatcher.executorService.shutdown()
        isConnected = false
    }

    override fun onOpen(ws: WebSocket, response: Response) {
        println("WebSocket connection opened")
        isConnected = true
    }

    override fun onMessage(ws: WebSocket, text: String) {
        coroutineScope.launch {
            try {
                _cryptoDataFlow.emit(gson.fromJson(text, CryptoPriceNetworkModel::class.java))
            } catch (e: JsonSyntaxException) {
                println("Failed to parse JSON message: $e")
            }
        }
    }

    override fun onFailure(ws: WebSocket, t: Throwable, response: Response?) {
        println("WebSocket error: $t")
        isConnected = false
    }

    override fun send(text: String) {
        if (!isConnected) start()
        webSocket?.send(text) ?: println("WebSocket is not connected.")
    }
}

val webSocketClient: CryptoWebSocketClient = DefaultCryptoWebSocketClient(
    url = CRYPTO_WEBSOCKET_CLIENT_TAG,
    gson = Gson()
)