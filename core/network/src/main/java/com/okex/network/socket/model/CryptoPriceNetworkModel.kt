package com.okex.network.socket.model

import com.okex.model.CryptoPriceResource

data class CryptoPriceNetworkModel(
    val arg: Arg,
    val `data`: List<Data>
)

fun CryptoPriceNetworkModel.toCryptoPriceResource() = CryptoPriceResource(
    symbol = this.data.first().instId,
    open24h = this.data.first().open24h,
    last = this.data.first().last
)