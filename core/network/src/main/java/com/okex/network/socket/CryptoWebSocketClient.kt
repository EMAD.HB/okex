package com.okex.network.socket

import com.okex.network.socket.model.CryptoPriceNetworkModel
import kotlinx.coroutines.flow.SharedFlow

interface CryptoWebSocketClient {
    fun start()
    fun stop()
    fun send(text: String)
    val cryptoDataFlow: SharedFlow<CryptoPriceNetworkModel>
}