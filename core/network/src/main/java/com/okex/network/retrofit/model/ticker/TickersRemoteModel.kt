package com.okex.network.retrofit.model.ticker

import com.okex.model.TickersResource

data class TickersRemoteModel(
    val code: Int,
    val msg: String,
    val tickers: List<Ticker>
)

fun TickersRemoteModel.toTickersResource() = mutableListOf<TickersResource>().apply {
    this@toTickersResource.tickers.forEach { ticker ->
        add(
            TickersResource(
                symbol = ticker.symbol,
                open24h = ticker.open_24h,
                last = ticker.last
            )
        )
    }
}