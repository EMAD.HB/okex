package com.okex.network.di

import com.okex.network.socket.CryptoWebSocketClient
import com.okex.network.socket.webSocketClient
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object WebSocketModule {

    @Provides
    @Singleton
    fun provideWebSocketClient(): CryptoWebSocketClient {
        return webSocketClient
    }

}