package com.okex.network.socket.model

data class Arg(
    val channel: String,
    val instId: String
)