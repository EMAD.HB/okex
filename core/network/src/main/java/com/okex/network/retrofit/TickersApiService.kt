package com.okex.network.retrofit

import com.okex.network.retrofit.model.ticker.TickersRemoteModel
import retrofit2.Response
import retrofit2.http.GET

interface TickersApiService {
    @GET("oapi/v1/market/tickers")
    suspend fun getTickers(): Response<TickersRemoteModel>
}