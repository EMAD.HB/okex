package com.okex.network.socket.constants

object CryptoWebSocketClientConstants {
    const val CRYPTO_WEBSOCKET_CLIENT_TAG = "wss://wsg.ok-ex.io/ws"
}