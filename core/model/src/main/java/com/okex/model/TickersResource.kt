package com.okex.model

data class TickersResource(
    val symbol: String,
    val open24h: String,
    val last: String
)