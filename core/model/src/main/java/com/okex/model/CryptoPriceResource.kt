package com.okex.model

data class CryptoPriceResource(
    val symbol: String,
    val open24h: String = "0.0",
    val last: String = "0.0"
)

fun TickersResource.toCryptoPriceResource() = CryptoPriceResource(
    symbol = this.symbol,
    last = this.last,
    open24h = this.open24h
)