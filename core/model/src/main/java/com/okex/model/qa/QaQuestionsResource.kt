package com.okex.model.qa

data class QaQuestionsResource(
    val question : String,
    val answer : String
)
