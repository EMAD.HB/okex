package com.okex.model.qa

data class QaResource(
    val sectionTitle : String,
    val questions : List<QaQuestionsResource>
)
