package com.okex.model

interface TokenPriceListener {
    fun onPrice(info : CryptoPriceResource)
    fun onError()
}