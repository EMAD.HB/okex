package com.okex.ui

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.rememberLottieComposition
import com.okex.designsystem.component.TextBodyLarge
import com.okex.designsystem.theme.TypographyType

@Composable
fun NoInternetConnection() {
    Column(
        Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        val composition by rememberLottieComposition(LottieCompositionSpec.RawRes(R.raw.not_internet_connection_animation))
        LottieAnimation(
            composition = composition,
            contentScale = ContentScale.Crop
        )
        22.ToVerticalSpace()
        TextBodyLarge(
            text = stringResource(R.string.no_internet_connection),
            color = MaterialTheme.colorScheme.onBackground,
            typographyType = TypographyType.ENGLISH
        )
    }
}