package com.okex.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Shapes
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextDirection
import androidx.compose.ui.unit.dp
import com.okex.designsystem.component.TextBodyMedium
import com.okex.designsystem.theme.DarkOnSuccess
import com.okex.designsystem.theme.DarkSuccess
import com.okex.designsystem.theme.LightOnSuccess
import com.okex.designsystem.theme.LightSuccess
import com.okex.designsystem.theme.TypographyType
import com.okex.model.CryptoPriceResource

@Composable
fun CryptoItem(ticker : CryptoPriceResource) {
    val priceChangeDirection = calculateChangeDirection(
        ticker.last.toDouble(),
        ticker.open24h.toDouble()
    )
    Box(
        modifier = Modifier.fillMaxWidth()
    ) {
        TextBodyMedium(
            modifier = Modifier.align(Alignment.CenterStart),
            text = ticker.symbol,
            typographyType = TypographyType.ENGLISH,
            color = MaterialTheme.colorScheme.onBackground
        )
        Box(
            modifier = Modifier
                .background(
                    color = when (priceChangeDirection) {
                        "-" -> MaterialTheme.colorScheme.errorContainer
                        "+" -> if (isSystemInDarkTheme()) DarkSuccess else LightSuccess
                        else -> if (isSystemInDarkTheme()) DarkSuccess else LightSuccess
                    },
                    shape = Shapes().small
                )
                .align(Alignment.Center)
                .padding(6.dp)
        ) {
            TextBodyMedium(
                text = ticker.cryptoPercentageChange(),
                typographyType = TypographyType.ENGLISH,
                direction = TextDirection.Ltr,
                color = when (priceChangeDirection) {
                    "-" -> MaterialTheme.colorScheme.onErrorContainer
                    "+" -> if (isSystemInDarkTheme()) DarkOnSuccess else LightOnSuccess
                    else -> if (isSystemInDarkTheme()) DarkOnSuccess else LightOnSuccess
                }
            )
        }
        TextBodyMedium(
            modifier = Modifier.align(Alignment.CenterEnd),
            text = ticker.last + " USDT ",
            direction = TextDirection.Ltr,
            typographyType = TypographyType.ENGLISH,
            color = MaterialTheme.colorScheme.onBackground
        )
    }
    26.ToVerticalSpace()
}