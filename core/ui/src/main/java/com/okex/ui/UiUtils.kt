package com.okex.ui

import com.okex.model.CryptoPriceResource

fun CryptoPriceResource.cryptoPercentageChange() : String {
    val currentPrice = this.last.toDouble()
    val price24HoursAgo = this.open24h.toDouble()
    val percentageChange = ((currentPrice - price24HoursAgo) / price24HoursAgo) * 100
    val priceChangeDirection = calculateChangeDirection(currentPrice, price24HoursAgo)
    return priceChangeDirection + "%.2f%%".format(percentageChange)
}

fun calculateChangeDirection(currentPrice : Double, price24HoursAgo : Double) : String {
    val percentageChange = ((currentPrice - price24HoursAgo) / price24HoursAgo) * 100
    return if (percentageChange >= 0) {
        "+"
    } else {
        "-"
    }
}